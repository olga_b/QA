package stefan.homework.files;

import java.io.*;
import java.util.HashSet;
import java.util.Scanner;

public class Main{
    public static void main(String[] args) throws IOException {

        replaceWords();

    }


    public static String toString(FileReader fr) throws FileNotFoundException {
        StringBuilder sb = new StringBuilder();
        try (Scanner scan = new Scanner(fr)) {
            while (scan.hasNextLine()) {
                sb.append(scan.nextLine()).append(System.lineSeparator());
            }
        }
        return sb.toString();
    }

    public static void replaceWords() throws IOException {
        FileReader fr = new FileReader("file.txt");
        FileReader fr2 = new FileReader("words.txt");
        FileWriter fw = new FileWriter("result.txt");
        Scanner scan = new Scanner(fr);
        Scanner scan2 = new Scanner(fr2);
        scan.useDelimiter("\\s");
        scan2.useDelimiter("\\s");
        StringBuilder sb = new StringBuilder();
        HashSet<String> hashSet = new HashSet<>();
        while(scan2.hasNext()) {
            hashSet.add(scan2.next());
        }
        while(scan.hasNext()){
            String str = scan.next().toLowerCase();
            if(hashSet.contains(str)){
                fw.write("CENSORED ");
            }
            else {
                fw.write(str);
                fw.write(" ");

            }
        fw.flush();
        }

    }
}
