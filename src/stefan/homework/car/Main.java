package stefan.homework.car;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Car car = new Car("Mini Cooper",3120,5.4f,false,10,40);
        System.out.println("Do you want to refuel? y/n:");
        Scanner scanner = new Scanner(System.in);
        String answer = scanner.next();
        if(answer.equals("y")){
            System.out.println("What amount of fuel do you want?");
            float litres = scanner.nextFloat();
            car.refuel(litres);
        }
        System.out.println("Do you want to drive? y/n");
        answer = scanner.next();
        if(answer.equals("y")){
            System.out.println("What distance do you want to drive?");
            float distance = scanner.nextFloat();
            car.drive(distance);
        }
    }

}
