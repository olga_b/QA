package stefan.homework.car;

public class Car {
    private String model;
    private float mileage;
    private float fuelConsumption;
    private boolean isStarted;
    private float currentFuelAmount;
    private float tankVolume;

    public Car(String model, float mileage, float fuelConsumption, boolean isStarted, float currentFuelAmount, float tankVolume) {
        this.currentFuelAmount = currentFuelAmount;
        this.fuelConsumption = fuelConsumption;
        this.isStarted = isStarted;
        this.mileage = mileage;
        this.model = model;
        this.tankVolume = tankVolume;

        System.out.println("Your car is " + model + ". It has " + mileage + " km mileage, " + tankVolume + " volume of tank, " + fuelConsumption + " fuel consumption.");
    }

    public float getCurrentFuelAmount() {
        return this.currentFuelAmount;
    }

    public float getTankVolume() {
        return this.tankVolume;
    }

    public float getFuelConsumption() {
        return this.fuelConsumption;
    }

    public String getModel() {
        return this.model;
    }

    public float getMileage() {
        return this.mileage;
    }

    public boolean getIsStarted() {
        return this.isStarted;
    }

    public void startCar() {
        if (this.currentFuelAmount < 1) {
            isStarted = false;
            System.out.println("You can't start a car. You have low fuel amount in a tank.");
        } else {
            isStarted = true;
            System.out.println("Car was successfully started.");
        }
    }

    public void refuel(float litres) {

        if ((currentFuelAmount + litres) <= tankVolume) {
            currentFuelAmount += litres;
        } else {
            currentFuelAmount = tankVolume;
        }
    }

    public void drive(float distance) {
        float availableDistance = currentFuelAmount * 100 / fuelConsumption;
        if (availableDistance > distance) {
            currentFuelAmount -= distance * fuelConsumption / 100;
            mileage += distance;
            System.out.println("You have driven " + distance + "km." +
                    " Your current fuel amount is " + currentFuelAmount +
                    " Your current mileage is " + mileage);
        } else {
            currentFuelAmount = 0;
            mileage += availableDistance;
            isStarted = false;
            System.out.println("You can't drive "+distance+" km. You have driven " + availableDistance + "km. " +
                    "Your current fuel amount is " + currentFuelAmount +
                    "Your current mileage is " + mileage+
                    "Your engine died out.");
        }

    }


}
