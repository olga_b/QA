package stefan.homework.recursion;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        /*System.out.println("Enter digit:");
        Scanner scan = new Scanner(System.in);
        int number = scan.nextInt();
        method(number);*/
        /*int[] array = new int[]{1,3,5,2,5};
        int index = method2(array, 3, 0);
        System.out.println("Your value is on "+index+" place.");*/
        //method3(10);
        /*int res = fibonacci(50);
        System.out.println(res);*/
        int[] array = new int[]{1,2,3,4};
//        permutations(array,0);

    }

    public static boolean method(int number) {
        int lastDigit = number % 10;
        System.out.print(lastDigit);
        number -= lastDigit;
        number /= 10;
        if (number == 0) {
            return false;
        } else {
            method(number);
        }
        return true;
    }

    public static int method2(int[] array, int value, int index) {
        if (index >= array.length) {
            return -1;
        }
        if (array[index] == value) {

            return index;
        } else {
            index++;
            return method2(array, value, index);
        }
    }

    public static void method3(int num) {
        if (num != 0) {
            method3(num - 1);
            System.out.print(num);
        } else {
            System.out.print(0);
        }

    }

    public static int fibonacci(int index) {

        if (index == 1) {
            return 0;
        }
        if (index == 2) {
            return 1;
        }

        return fibonacci(index - 1) + fibonacci(index - 2);

    }

    public static void permutations(int[] array, int i){
        if(i<array.length) {
            int temp = array[0];
            array[0] = array[i];
            array[i] = temp;
            for(int j:array){
                System.out.print(array[j]);
            }
            System.out.println("");
            permutations(array, i);
        }
        i++;


    }



}
