package homework.words;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Map;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws FileNotFoundException{
//
//        WordCounter wordCounter = new WordCounter();
//        wordCounter.count( " ","ты");

        FileReader fr = new FileReader("vim1.txt");
        Scanner scanner = new Scanner(fr);

        StringBuilder sb = new StringBuilder();

        while (scanner.hasNextLine()){
            sb.append(scanner.nextLine()).append(System.lineSeparator());
        }
        String str = sb.toString();

//        int result = WordCounter.count(str, "ты");
//        System.out.println(result);

        Map<String, Integer> result = WordCounter.count(str);
        int count = 0;
        for(Map.Entry e : result.entrySet()){
            count++;
            System.out.println(count + " " + e.getKey() + " : " + e.getValue());
        }
    }
}
