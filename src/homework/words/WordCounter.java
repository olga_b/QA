package homework.words;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class WordCounter {
//
//    private String string = "";
//    private ArrayList<String> arrayLists = new ArrayList<>();

    //FileReader file = new FileReader("/home/olya/IdeaProjects/Search_in_file/file.txt");

//    public static void count(String separator, String wantedWord) throws FileNotFoundException{
//        int counter = 0;
//        FileReader file = new FileReader("file.txt");
//        Scanner scanner = new Scanner(file);
//        if(scanner.hasNextLine()){
//            arrayLists.add(scanner.nextLine());
//            for(String line:arrayLists){
//                for(String word:line.split(separator)) {
//                    //arrayLists.add(word);
//                    if(word.equals(wantedWord)) {
//                        counter++;
//                    }
//                }
//            }
//        }
//
//        System.out.println("Word '" + wantedWord + "' was encountered " + counter + " times");
//        //System.out.println(path);
//    }

    public static int count(String text, String word){
        Scanner scan = new Scanner(text);
        int count = 0;
        scan.useDelimiter("[-:;\\s.,!]+");
        while(scan.hasNext()){
            String w = scan.next();
            if(w.equals(word)){
                count++;
            }
        }

        return count;
    }

    public static Map<String, Integer> count(String text){
        Scanner scan = new Scanner(text);
        scan.useDelimiter("[-:;\\s\\[\\]().,!?…»]+");
        Map<String, Integer> result = new HashMap<>();
        while(scan.hasNext()){
            String w = scan.next().toLowerCase();
            if(result.containsKey(w)){
                result.put(w, result.get(w) + 1);
            } else {
                result.put(w, 1);
            }
        }
        return result;
    }

}
