package homework.runners;

/**
 * Created by olya on 28.11.17.
 */
public class Result {


    public static int[] find(String[] names, int[] times){
        int temp = times[0];
        int [] ind = new int[]{0,0};
        for(int i=1; i<times.length; i++){
            ind[1] = ind[0];
            if(times[i]<temp){
                temp = times[i];
                ind[0] = i;
            }
        }
        return ind;
    }
}
