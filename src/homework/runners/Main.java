package homework.runners;

/**
 * Created by olya on 28.11.17.
 */
public class Main {
    public static void main(String[]args){

        String [] names = {"John", "Olga", "Vasyl", "Petro", "Jack", "Olena", "Mark", "Stefan"};
        int [] times = {40, 25, 33, 65, 44, 50, 33, 22};
        int[] position = Result.find(names,times);
        int first = position[0];
        int second = position[1];
        System.out.println("The fastest runner is " +names[first]);
        System.out.println("The second runner is " +names[second]);

    }
}
