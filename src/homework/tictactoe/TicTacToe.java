package homework.tictactoe;

import java.util.Random;

/**
 * Created by olya on 05.12.17.
 */
public class TicTacToe {

    Random rnd = new Random(System.currentTimeMillis());
    private final int n = 3;
    private final int m = 3;
    private final int[][] comb = {
            {0, 1, 2},
            {3, 4, 5},
            {6, 7, 8},
            {0, 3, 6},
            {1, 4, 7},
            {2, 5, 8},
            {0, 4, 8},
            {2, 4, 6}
    };

    private char[][] field = {
            {
                    '-', '-', '-'
            },
            {
                    '-', '-', '-'
            },
            {
                    '-', '-', '-'
            }
    };

    public void printField() {

        for (int i = 0; i < n; i++) {
            System.out.println(System.lineSeparator());
            for (int j = 0; j < m; j++) {
                System.out.print(this.field[i][j] + "  ");
            }
        }
    }

    public boolean fieldHasEmptyCell() {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                if (this.field[i][j] == '-') {
                    return false;
                }
            }
        }
        System.out.println("\nGame is over.");
        return true;
    }

    public void humanMove(int index) {
        if (this.field[index / 3][index % 3] == '-') {
            this.field[index / 3][index % 3] = 'O';
        }
    }

    public void compMove() {
        int index;
        do {
            index = rnd.nextInt(9);
        }
        while (!(this.field[index / 3][index % 3] == '-'));
        this.field[index / 3][index % 3] = 'X';
    }

    public boolean hasWinner() {
        for (int i = 0; i < 8; i++) {
            if ((field[comb[i][0] / 3][comb[i][0] % 3] == field[comb[i][1] / 3][comb[i][1] % 3]) &&
                    (field[comb[i][1] / 3][comb[i][1] % 3] == field[comb[i][2] / 3][comb[i][2] % 3])) {
                if (field[comb[i][0] / 3][comb[i][0] % 3] == 'O') {
                    System.out.println("\nWinner is O");
                    return true;
                }
                if (field[comb[i][0] / 3][comb[i][0] % 3] == 'X') {
                    System.out.println("\nWinner is X");
                    return true;
                }
            }

        }
        return false;
    }
}

