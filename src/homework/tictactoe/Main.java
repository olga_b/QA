package homework.tictactoe;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Main{

    public static void main(String[] args) {

    findWinner();

    }

    public static boolean findWinner(){
        TicTacToe ticTacToe = new TicTacToe();
        Scanner scan = new Scanner(System.in);

         do{
            ticTacToe.printField();
            System.out.println("\nВаш ход:");
            int index = scan.nextInt();
            ticTacToe.humanMove(index-1);
            ticTacToe.printField();
            if(ticTacToe.hasWinner() || ticTacToe.fieldHasEmptyCell()){
                return true;
            }
            ticTacToe.compMove();
            ticTacToe.printField();
        }while(!ticTacToe.hasWinner() && !ticTacToe.fieldHasEmptyCell());

        return false;
    }
}
