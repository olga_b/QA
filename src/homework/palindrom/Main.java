package homework.palindrom;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        System.out.println("Введите число:");
        int num = scan.nextInt();
        boolean bool = Palindrom.isPalindrom(num);

        if(bool){
            System.out.println("Введенное число - палиндром");
        } else {
            System.out.println("Введенное число не является палиндромом");
        }

    }
}
