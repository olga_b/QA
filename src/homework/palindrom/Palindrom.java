package homework.palindrom;

import java.util.ArrayList;

public class Palindrom {

    public static boolean isPalindrom(int num) {
        ArrayList<Integer> arrayList = new ArrayList<>();

        while (num != 0) {
            int lastDigit = num % 10;
            arrayList.add(lastDigit);  //1 2 3 2 1
            num -= lastDigit;   //12320 1230 120 10 0
            num /= 10;    //1232 123 12 1 0
        }
        for (int i = 0; i < arrayList.size() / 2; i++) {  //12321  1 1 2 2 3 3
            if (!arrayList.get(i).equals(arrayList.get(arrayList.size() - 1 - i))) {
                return false;
            }
        }

        return true;
    }
}
