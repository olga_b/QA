package homework.printDigit;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число:");
        int number = scanner.nextInt();
        StarNumber n = new StarNumber(number);
        System.out.println(n);
    }
}
