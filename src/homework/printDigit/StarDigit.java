package homework.printDigit;

import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;

public class StarDigit {
    private static final String [][] DIGITS = {
            {
                    " *** ",
                    "*   *",
                    "*   *",
                    "*   *",
                    " *** ",
            },
            {
                    "    *",
                    "   **",
                    "  * *",
                    "    *",
                    "    *",
            },
            {
                    " ****",
                    "*   *",
                    "  ** ",
                    "*    ",
                    "*****",
            },
            {
                    "*****",
                    "   * ",
                    "  *  ",
                    "    *",
                    "*****",
            },
            {
                    "*   *",
                    "*   *",
                    "*****",
                    "    *",
                    "    *",
            },
            {
                    "*****",
                    "*    ",
                    "*****",
                    "    *",
                    "*****",
            },
            {
                    "*****",
                    "*    ",
                    "*****",
                    "*   *",
                    "*****",
            },
            {
                    "*****",
                    "   **",
                    "  *  ",
                    "*    ",
                    "*    ",
            },
            {
                    "*****",
                    "*   *",
                    "*****",
                    "*   *",
                    "*****",
            },
            {
                    "*****",
                    "*   *",
                    "*****",
                    "    *",
                    "*****",
            },
    };

    private String [] digit;

    public StarDigit(int inDigit){
        this.digit = DIGITS[inDigit];
    }

    public String [] getDigit(){
        return digit.clone();
    }

    public void printDigit(PrintStream writer){

        for(String s: this.digit){
            writer.println(s);
        }


    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for(String s: this.digit){
            sb.append(s).append(System.lineSeparator());
        }
        return  sb.toString();
    }
}
