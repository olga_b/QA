package homework.printDigit;

import java.util.ArrayList;

public class StarNumber {
    private ArrayList<StarDigit> digits = new ArrayList<>();

    
    public StarNumber(int number){
        while (number != 0) {
            int index = number % 10;
            StarDigit digit = new StarDigit(index);
            digits.add(0,digit);
            number -= index;
            number /= 10;
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < 5; i++){
            for(StarDigit digit: digits){
                String [] arr = digit.getDigit();
                sb.append(arr[i]).append("   ");
            }
            sb.append(System.lineSeparator());
        }
        String result = sb.toString();
      return result;
    }
}
