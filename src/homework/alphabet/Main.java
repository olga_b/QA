package homework.alphabet;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by olya on 28.11.17.
 */
public class Main {
    public static void main(String[] args) throws IOException {
        ArrayList<String> list = new ArrayList<>();
        FileReader fr = new FileReader("file.txt");
        Scanner scan = new Scanner(fr);

        StringBuilder sb = new StringBuilder();
        while(scan.hasNextLine()){
            sb.append(scan.nextLine()).append(System.lineSeparator());
        }
        String str = sb.toString();
        list = Sort.sort(str);
        Sort.write(list,"result.txt" );

    }
}
