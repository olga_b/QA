package homework.alphabet;


import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by olya on 28.11.17.
 */
public class Sort {


    public static ArrayList <String> sort(String text) {
        Scanner scan = new Scanner(text);
        scan.useDelimiter("[-:;\\s.,!]+");
        ArrayList<String> list = new ArrayList<>();
        while (scan.hasNext()) {
            list.add(scan.next());
        }
        alphabeticSort(list);

/*        StringBuilder sb = new StringBuilder();
        for(String word:list){
           sb.append(word).append(System.lineSeparator());
        }
        String result = sb.toString();*/

        return list;

    }

    public static void alphabeticSort(ArrayList<String> array) {
        String temp = "";
        for (int i = 0; i < array.size() - 1; i++) {
            for (int j = 0; j < array.size() - i - 1; j++) {
                if ((array.get(j).compareTo(array.get(j + 1))) > 0) {
                    temp = array.get(j + 1);
                    array.set(j + 1, array.get(j));
                    array.set(j, temp);
                }
            }
        }
    }
    public static void write(ArrayList<String> array, String filePath) throws IOException{

        FileWriter fw = new FileWriter(filePath);

        for(String word: array){
            fw.write(word);
            fw.append("\n");
        }
        fw.flush();
    }
}
